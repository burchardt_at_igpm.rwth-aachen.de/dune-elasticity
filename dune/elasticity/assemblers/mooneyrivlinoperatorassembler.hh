#ifndef MOONEY_RIVLIN_OPERATOR_ASSEMBLER_HH
#define MOONEY_RIVLIN_OPERATOR_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/elasticity/common/elasticityhelpers.hh>

/** \brief Assemble the second derivative of a Mooney-Rivlin material.
 *
 *    \f[
 *      W(u)= a tr(E(u)) + b tr(E(u))^2 + c ||E(u)||^2 + d J^2 + e J^-k,  k>=2  
 *    \f]
 *
 *   where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$\a\f$,...,\f$\e\f$ material parameters
 *      - \f$k\f$ controlls orientation preservation (k \approx \floor(1/(1-2nu) -1))
*/  
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class MooneyRivlinOperatorAssembler 
    : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<typename GridType::ctype ,GridType::dimension,GridType::dimension> >
{
    static constexpr int dim = GridType::dimension;
    using ctype = typename GridType::ctype;
    using GridFunction = VirtualGridFunction<GridType, Dune::FieldVector<ctype, dim> >;

    using T = Dune::FieldMatrix<ctype, dim, dim>;
    using Base = LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T>;
public:

    using typename Base::Element;
    using typename Base::BoolMatrix;
    using typename Base::LocalMatrix;

    MooneyRivlinOperatorAssembler() = default;

    MooneyRivlinOperatorAssembler(ctype a, ctype b, ctype c, ctype d, ctype e, int k) :
        a_(a),b_(b),c_(c),d_(d),e_(e),k_(k)
    {}

    void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        using FVector = Dune::FieldVector<ctype, dim>;
        using JacobianType = typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType;

        int rows = tFE.localBasis().size();
        int cols = aFE.localBasis().size();

        localMatrix.setSize(rows,cols);
        localMatrix = 0.0;

        // get quadrature rule (should depend on k?)
        const int order = (element.type().isSimplex()) 
            ? 6*(tFE.localBasis().order())
            : 6*(tFE.localBasis().order()*dim);
        const auto& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE));

        // store gradients of reference and element shape functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<FVector> gradients(tFE.localBasis().size());

        const auto& geometry = element.geometry();

        // loop over quadrature points
        for (const auto& pt : quad) {

            const auto& quadPos = pt.position();

            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);
            auto integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute transformed gradients of element basis functions
            for (size_t i=0; i < gradients.size(); ++i)
                invJacobian.mv(referenceGradients[i][0], gradients[i]);

            // evaluate displacement gradients the configuration at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (displacement_->isDefinedOn(element))
                displacement_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                displacement_->evaluateDerivative(geometry.global(quadPos), localConfGrad);

            // compute linearization of the determinante of the deformation gradient
            auto linDefDet = Dune::Elasticity::linearisedDefDet(localConfGrad);

            // compute deformation gradient of the configuration
            Dune::MatrixVector::addToDiagonal(localConfGrad, 1.0);

            auto J = localConfGrad.determinant();

            //compute linearized strain for all shapefunctions
            std::vector<std::array<T, dim > > defGrad(rows);
            std::vector<std::array<SymmetricTensor<dim>, dim> > linStrain(rows);

            for (int i=0; i<rows; i++)
                for (int k=0; k<dim; k++) {

                    // The deformation gradient of the shape function
                    defGrad[i][k] = 0;
                    defGrad[i][k][k] = gradients[i];

                    // The linearized strain is the symmetric product of defGrad and (Id+localConf)
                    linStrain[i][k] = Dune::Elasticity::linearisedStrain(defGrad[i][k], localConfGrad);
                }

            auto strain = Dune::Elasticity::strain(localConfGrad);
            ctype z = pt.weight() * integrationElement;

            ctype coeff{(2 * d_ * J - k_ * e_ * std::pow(J,-k_-1)) * z};
            ctype coeff2{(2 * d_ + k_ * (k_ + 1) * e_*std::pow(J,-k_-2)) * z};

            for (int row = 0; row < rows; ++row)
                for (int col = 0; col<cols; ++col) {
                        
                  // second derivative of the trace terms
                  auto hessTrace = z*(a_ + 2*b_*strain.trace()) * ((gradients[row]*gradients[col]));
                  Dune::MatrixVector::addToDiagonal(localMatrix[row][col], hessTrace);
                  // second derivative of the determinat term
                  localMatrix[row][col].axpy(coeff, hessDefDet(localConfGrad,gradients[row],gradients[col]));

                  FVector rowTerm(0),colTerm(0);
                  linDefDet.mv(gradients[row], rowTerm);
                  linDefDet.mv(gradients[col], colTerm);

                  FVector traceRowTerm(0),traceColTerm(0);
                  localConfGrad.mv(gradients[row], traceRowTerm);
                  localConfGrad.mv(gradients[col], traceColTerm);

                  // second derivative of the scalar product term
                  for (int rcomp=0; rcomp < dim; rcomp++) {
                    // derivative of the coefficient term in the compressibility function
                    localMatrix[row][col][rcomp].axpy(coeff2 * rowTerm[rcomp], colTerm);

                    // second derivative of the trace terms part 2
                    localMatrix[row][col][rcomp].axpy(2*z*b_*traceRowTerm[rcomp], traceColTerm);

                    // derivative of the scalar product terms
                    for (int ccomp=0; ccomp<dim; ccomp++) {
                      localMatrix[row][col][rcomp][ccomp] += (linStrain[row][rcomp]*linStrain[col][ccomp])*2*c_*z;
                      auto linScp = Dune::Elasticity::linearisedStrain(defGrad[row][rcomp], defGrad[col][ccomp]);
                      localMatrix[row][col][rcomp][ccomp] += (strain*linScp)*z*2*c_;
                    }
                  }
                }
        }
    }

    /** \brief Set new configuration to be assembled at. Needed e.g. during Newton iteration.*/
    void setConfiguration(std::shared_ptr<GridFunction> newConfig) {
        displacement_ = newConfig;
    }

private:

    //! Material parameters
    ctype a_; ctype b_; ctype c_; ctype d_; ctype e_;
    int k_;

    /** \brief The configuration at which the linearized operator is evaluated.*/
    std::shared_ptr<GridFunction> displacement_;


    /** \brief Compute the matrix entry that arises from the second derivative of the determinant. 
     *  Note that it is assumed the defGrad is the deformation gradient and not the displacement gradient   
     */
    auto hessDefDet(const Dune::FieldMatrix<ctype,dim,dim>& defGrad,
                    const Dune::FieldVector<ctype,dim>& testGrad, const Dune::FieldVector<ctype,dim>& ansatzGrad) const
    {
        Dune::FieldMatrix<ctype, dim, dim> res(0);
        
        if (dim==2) {
            res[0][1] = testGrad[0]*ansatzGrad[1]-ansatzGrad[0]*testGrad[1];
            res[1][0] = -res[0][1];
            return res;
        }

        //compute the cross product
        Dune::FieldVector<ctype,dim> cross(0);    
        for (int k=0; k<dim; k++)
            cross[k]=testGrad[(k+1)%dim]*ansatzGrad[(k+2)%dim] -testGrad[(k+2)%dim]*ansatzGrad[(k+1)%dim];

        // the resulting matrix is skew symmetric with entries <cross,degGrad[i]>
        for (int i=0; i<dim; i++)
            for (int j=i+1; j<dim; j++) {
                int k= (dim-(i+j))%dim;
            res[i][j] = (cross*defGrad[k])*std::pow(-1,k);
            res[j][i] = -res[i][j];
        }
        
        return res;
    }
};


#endif

