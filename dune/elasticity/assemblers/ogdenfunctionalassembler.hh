// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_OGDEN_FUNCTIONAL_ASSEMBLER_HH
#define DUNE_ELASTICITY_OGDEN_FUNCTIONAL_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include "dune/fufem/assemblers/localfunctionalassembler.hh"
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/elasticity/common/elasticityhelpers.hh>

/** \brief Local assembler for the linearization of an Ogden energy at a displacement u
 *
 *
 *    \f[
 *      W(\nabla\varphi)= a tr E(\varphi) + b (tr E(\varphi))^2 + c tr(E(\varphi)^2) + d\Gamma(J)
 *    \f]
 *
 *  where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$\Gamma(x)=-log(x)\f$: or \f$\Gamma(x)=x^2-log(x)\f$ the penalty function
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$a\f$,\f$b\f$,\f$c\f$,\f$d\f$ material parameters
 *  with
 *       a = -d*Gamma'(1)
 *       b = (\lambda - d*(Gamma'(1)+Gamma''(1)))/2
 *       c = \mu + d*Gamma'(1)
 *       d > 0
 *
 *
 *  The linearization of this energy at \f$ u\f$ as a functional applied to a test function \f$ v\f$] reads
 *
 *    \f[
 *      l_u(v) := (\frac{\lambda}2 J - \frac{\frac{\lambda}2 + \mu}{J})\frac{\partial J}{\partial u}v +
 *                  \mu\frac{\partial tr(E(u))}{\partial u} v
 *    \f]
 *
*/
template <class GridType, class TrialLocalFE>
class OgdenFunctionalAssembler :
    public LocalFunctionalAssembler<GridType, TrialLocalFE, Dune::FieldVector<typename GridType::ctype ,GridType::dimension> >

{
    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype,GridType::dimension> T;

public:
    typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::Element Element;
    typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::LocalVector LocalVector;
    typedef VirtualGridFunction<GridType, T > GridFunction;

    //! Create assembler from material parameters and a grid
    OgdenFunctionalAssembler(ctype lambda, ctype mu, ctype d, std::shared_ptr<GridFunction> displacement) :
        d_(d),displacement_(displacement)
    {
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }

    //! Create assembler from material parameters
    OgdenFunctionalAssembler(ctype lambda, ctype mu, ctype d) :
        d_(d)
    {
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }


    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    {
        typedef Dune::FieldMatrix<ctype,dim,dim> FM;
        typedef typename Element::Geometry::JacobianInverseTransposed JacInvTransType;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        int rows = tFE.localBasis().size();

        localVector.resize(rows);
        localVector = 0.0;

        // TODO get proper quadrature rule
        const int order = (element.type().isSimplex())
            ? 3*(tFE.localBasis().order())
            : 3*(tFE.localBasis().order()*dim);

        // get quadrature rule
        const Dune::QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<T> gradients(tFE.localBasis().size());

        // store geometry mapping of the entity
        const typename Element::Geometry geometry = element.geometry();

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const LocalCoordinate& quadPos = quad[pt].position();

            // get transposed inverse of Jacobian of transformation
            const JacInvTransType& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            /* Compute the weight of the current integration point */
            ctype z = quad[pt].weight() * integrationElement;

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradients of base functions
            for (size_t i=0; i<gradients.size(); ++i) {

                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);

            }

            // evaluate the displacement gradient at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (displacement_->isDefinedOn(element))
                displacement_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                displacement_->evaluateDerivative(geometry.global(quadPos),localConfGrad);

            // compute linearization of the determinante of the deformation gradient
            FM linDefDet;
            Dune::Elasticity::linearisedDefDet(localConfGrad,linDefDet);

            SymmetricTensor<dim,ctype> strain;
            Dune::Elasticity::strain(localConfGrad,strain);

            // Compute tr(E)
            ctype trE = strain.trace();

            // make deformation gradient out of the discplacement
            for (int i=0;i<dim;i++)
                localConfGrad[i][i] += 1;

            // evaluate the derminante of the deformation gradient
            const ctype J = localConfGrad.determinant();

            for (int i=0;i<dim;i++)
                localConfGrad[i][i] -= 1;

            // derivative of the penalty term at J
            const ctype dGamma = Dune::Elasticity::Gamma_x(J);


            /****************************************************/
            /* The deformation gradients of the shape functions */
            /****************************************************/

            Dune::FieldMatrix<ctype,dim,dim> nablaPhi[100*dim];
            for (int dof=0; dof<rows; dof++)
                for (int comp=0; comp<dim; comp++)
                    for (int i=0; i<dim; i++)
                        for (int j=0; j<dim; j++)
                            nablaPhi[dof*dim+comp][i][j] = (i==comp) ? gradients[dof][j] : 0;

            /********************************************************/
            /* Compute first derivative of Ogden Energy functional  */
            /********************************************************/

            Dune::FieldMatrix<double,dim,dim> fu;
            ogdenLinearised(localConfGrad,strain,trE,dGamma,fu);

            for (int i=0; i<rows*dim; i++) {

                for (int k=0; k<dim; k++)
                    for (int l=0; l<dim; l++)
                        localVector[i/dim][i%dim] += nablaPhi[i][k][l] * fu[k][l] * z;
            }
        }
    }

    /** \brief Set new configuration. In Newton iterations this needs to be assembled more than one time.*/
    void setConfiguration(std::shared_ptr<GridFunction> newDisplacement) {
        displacement_ = newDisplacement;
    }

private:
    /*----------------------------------------------------------------------------*/
    void ogdenLinearised(const Dune::FieldMatrix<ctype,2,2>& u,
                         const SymmetricTensor<2>& strain, ctype trE, ctype dGamma,
                         Dune::FieldMatrix<ctype,2,2>& fu) const {

        fu[0][0] = compute_fu<0,0>(u,strain,trE,dGamma);
        fu[1][0] = compute_fu<1,0>(u,strain,trE,dGamma);
        fu[0][1] = compute_fu<0,1>(u,strain,trE,dGamma);
        fu[1][1] = compute_fu<1,1>(u,strain,trE,dGamma);
    }

    void ogdenLinearised(const Dune::FieldMatrix<ctype,3,3>& u,
                         const SymmetricTensor<3>& strain, ctype trE, ctype dGamma,
                         Dune::FieldMatrix<ctype,3,3>& fu) const {

        fu[0][0] = compute_fu<0,0>(u,strain,trE,dGamma);
        fu[1][0] = compute_fu<1,0>(u,strain,trE,dGamma);
        fu[2][0] = compute_fu<2,0>(u,strain,trE,dGamma);
        fu[0][1] = compute_fu<0,1>(u,strain,trE,dGamma);
        fu[1][1] = compute_fu<1,1>(u,strain,trE,dGamma);
        fu[2][1] = compute_fu<2,1>(u,strain,trE,dGamma);
        fu[0][2] = compute_fu<0,2>(u,strain,trE,dGamma);
        fu[1][2] = compute_fu<1,2>(u,strain,trE,dGamma);
        fu[2][2] = compute_fu<2,2>(u,strain,trE,dGamma);
    }


    template <int p, int q>
    ctype compute_fu(const Dune::FieldMatrix<ctype,2,2>& u,
                     const SymmetricTensor<2,ctype>& E,
                     const ctype& trE, const ctype& dGamma) const
    {
        ctype trE_dupq(0);
        trE_dupq += Dune::Elasticity::E_du<0,0,p,q>(u);
        trE_dupq += Dune::Elasticity::E_du<1,1,p,q>(u);

        ctype tmp(0);
        tmp += E(0,0)*Dune::Elasticity::E_du<0,0,p,q>(u);
        tmp += E(0,1)*Dune::Elasticity::E_du<0,1,p,q>(u);
        tmp += E(1,0)*Dune::Elasticity::E_du<1,0,p,q>(u);
        tmp += E(1,1)*Dune::Elasticity::E_du<1,1,p,q>(u);

        return a_*trE_dupq + 2*b_*trE*trE_dupq + 2*c_*tmp + d_*dGamma*Dune::Elasticity::det_du_tmpl<p,q>(u);
    }

    template <int p, int q>
    ctype compute_fu(const Dune::FieldMatrix<ctype,3,3>& u,
                     const SymmetricTensor<3,ctype>& E,
                     const ctype& trE, const ctype& dGamma) const
    {
        ctype trE_dupq(0);
        trE_dupq += Dune::Elasticity::E_du<0,0,p,q>(u);
        trE_dupq += Dune::Elasticity::E_du<1,1,p,q>(u);
        trE_dupq += Dune::Elasticity::E_du<2,2,p,q>(u);

        ctype tmp(0);

        tmp += E(0,0)*Dune::Elasticity::E_du<0,0,p,q>(u);
        tmp += E(0,1)*Dune::Elasticity::E_du<0,1,p,q>(u);
        tmp += E(0,2)*Dune::Elasticity::E_du<0,2,p,q>(u);
        tmp += E(1,0)*Dune::Elasticity::E_du<1,0,p,q>(u);
        tmp += E(1,1)*Dune::Elasticity::E_du<1,1,p,q>(u);
        tmp += E(1,2)*Dune::Elasticity::E_du<1,2,p,q>(u);
        tmp += E(2,0)*Dune::Elasticity::E_du<2,0,p,q>(u);
        tmp += E(2,1)*Dune::Elasticity::E_du<2,1,p,q>(u);
        tmp += E(2,2)*Dune::Elasticity::E_du<2,2,p,q>(u);

        return a_*trE_dupq + 2*b_*trE*trE_dupq + 2*c_*tmp + d_*dGamma*Dune::Elasticity::det_du_tmpl<p,q>(u);
    }

    //! The Ogden material parameter
    ctype a_;
    //! The Ogden material parameter
    ctype b_;
    //! The Ogden material parameter
    ctype c_;
    //! The Ogden material parameter
    ctype d_;

    /** \brief The configuration at which the functional is evaluated.*/
    std::shared_ptr<GridFunction> displacement_;
};

#endif


