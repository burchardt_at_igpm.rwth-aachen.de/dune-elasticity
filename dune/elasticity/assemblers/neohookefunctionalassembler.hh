#ifndef NEO_HOOKE_FUNCTIONAL_ASSEMBLER_HH
#define NEO_HOOKE_FUNCTIONAL_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include "dune/fufem/assemblers/localfunctionalassembler.hh"
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/elasticity/common/elasticityhelpers.hh>

/** \brief Local assembler for the linearization of a nonlinear Neo-Hookean energy at a displacement u
 *
 *  Laursen:
 *    \f[
 *      W(u)= \frac{\lambda}4(J^2-1)-(\frac{\lambda}2+\mu)log(J)+\mu tr(E(u))
 *    \f]
 *
 *  Fischer/Wriggers:
 *    \f[
 *      W(u)= \frac{\lambda}2(J-1)^2 - 2 \mu*log(J)+\mu tr(E(u))
 *    \f]
 *
 *  The linearization of this energy at \f$ u\f$ as a functional applied to a test function \f$ v\f$] reads
 *
 *  
 *  Laursen:
 *    \f[
 *      l_u(v) := (\frac{\lambda}2 J - \frac{\frac{\lambda}2 + \mu}{J})\frac{\partial J}{\partial u}v + 
 *                  \mu\frac{\partial tr(E(u))}{\partial u} v
 *    \f]
 *
 *  Fischer:
 *    \f[
 *      l_u(v) := (\lambda (J-1) - \frac{2\mu}{J})\frac{\partial J}{\partial u}v +
                   \mu\frac{\partial tr(E(u))}{\partial u} v
 *    \f]
 *
 *   where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$\lambda\f$,\f$\mu\f$ material parameters (first lame and shear modulus)
*/ 
template <class GridType, class TrialLocalFE>
class NeoHookeFunctionalAssembler : 
    public LocalFunctionalAssembler<GridType, TrialLocalFE, Dune::FieldVector<typename GridType::ctype ,GridType::dimension> >

{
    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype,GridType::dimension> T;

public:
    typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::Element Element;
    typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE,T>::LocalVector LocalVector;
    typedef VirtualGridFunction<GridType, T > GridFunction;

    NeoHookeFunctionalAssembler() = default;

    //! Create assembler from material parameters and a grid
    NeoHookeFunctionalAssembler(ctype lambda, ctype mu, const std::shared_ptr<GridFunction> displacement) :
        lambda_(lambda),
        mu_(mu),
        displacement_(displacement)
    {}

    //! Create assembler from material parameters
    NeoHookeFunctionalAssembler(ctype lambda, ctype mu) :
        lambda_(lambda),
        mu_(mu)
    {}


    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    {
        typedef Dune::FieldMatrix<ctype,dim,dim> FMdimdim;
        typedef typename Element::Geometry::JacobianInverseTransposed JacInvTransType;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;    
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate; 

        int rows = tFE.localBasis().size();

        localVector.resize(rows);
        localVector = 0.0;

        // TODO get proper quadrature rule
        const int order = (element.type().isSimplex())
            ? 4*(tFE.localBasis().order())
            : 4*(tFE.localBasis().order()*dim);

        // get quadrature rule
        const Dune::QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<T> gradients(tFE.localBasis().size());

        // store geometry mapping of the entity
        const typename Element::Geometry geometry = element.geometry();

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const LocalCoordinate& quadPos = quad[pt].position();

            // get transposed inverse of Jacobian of transformation
            const JacInvTransType& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradients of base functions
            for (size_t i=0; i<gradients.size(); ++i) {

                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);

            }

            // evaluate the displacement gradient at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (displacement_->isDefinedOn(element))
                displacement_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                displacement_->evaluateDerivative(geometry.global(quadPos),localConfGrad);

            // compute linearization of the determinante of the deformation gradient
            FMdimdim linDefDet;
            Dune::Elasticity::linearisedDefDet(localConfGrad,linDefDet);

            // make deformation gradient out of the discplacement
            for (int i=0;i<dim;i++)
                localConfGrad[i][i] += 1;        

            // evaluate the derminante of the deformation gradient
            const ctype J = localConfGrad.determinant();

            // collect terms 
            FMdimdim fu(0);

#ifdef LAURSEN
            // Laursen
            fu.axpy(J*lambda_/2-(lambda_/2+mu_)/J,linDefDet);
#else
            // Fischer
            fu.axpy((J-1)*lambda_-2*mu_/J,linDefDet);
#endif
            // the linearization of the trace is just deformation gradient
            fu.axpy(mu_,localConfGrad);

            ctype z = quad[pt].weight()*integrationElement;

            // add vector entries
            for(int row=0; row<rows; row++) 
                fu.usmv(z,gradients[row],localVector[row]);
        }
    }

    /** \brief Set new configuration. In Newton iterations this needs to be assembled more than one time.*/
    void setConfiguration(std::shared_ptr<GridFunction> newDisplacement) {
        displacement_ = newDisplacement;
    }

private:
    /** \brief Lame constant */
    ctype lambda_;

    /** \brief Lame constant */
    ctype mu_;

    /** \brief The configuration at which the functional is evaluated.*/
    std::shared_ptr<GridFunction> displacement_;
};

#endif


