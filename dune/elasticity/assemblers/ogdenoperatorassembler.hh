// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_OGDEN_OPERATOR_ASSEMBLER_HH
#define DUNE_ELASTICITY_OGDEN_OPERATOR_ASSEMBLER_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/elasticity/common/elasticityhelpers.hh>

/** \brief Assemble the second derivative of a neo-hookean material.
 *
 *  Laursen:
 *    \f[
 *      W(u)= \frac{\lambda}4(J^2-1)-(\frac{\lambda}2+\mu)log(J)+\mu tr(E(u))
 *    \f]
 *
 *  Fischer/Wriggers:
 *    \f[
 *      W(u)= \frac{\lambda}2(J-1)^2-2\mu)log(J)+\mu tr(E(u))
 *    \f]
 *
 *  The linearization of this energy at \f$ u\f$ as a functional applied to a test function \f$ v\f$] reads
 *
 *
 *  Laursen:
 *    \f[
 *      l_u(v) := (\frac{\lambda}2 J - \frac{\frac{\lambda}2 + \mu}{J})\frac{\partial J}{\partial u}v +
 *                  \mu\frac{\partial tr(E(u))}{\partial u} v
 *    \f]
 *
 *  Fischer:
 *    \f[
 *      l_u(v) := (\lambda (J-1) - \frac{2\mu}{J})\frac{\partial J}{\partial u}v +
 *                  \mu\frac{\partial tr(E(u))}{\partial u} v
 *    \f]
 *
 *   where
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$tr \f$: the trace operator
 *      - \f$J\f$ the determinant of the deformation gradient
 *      - \f$\lambda\f$,\f$\mu\f$ material parameters (first lame and shear modulus)
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class OgdenOperatorAssembler
    : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<typename GridType::ctype ,GridType::dimension,GridType::dimension> >
{


    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;

    typedef VirtualGridFunction<GridType, Dune::FieldVector<ctype,GridType::dimension> > GridFunction;

public:
    typedef typename Dune::FieldMatrix<ctype,GridType::dimension,GridType::dimension> T;

    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::Element Element;
    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE,T >::BoolMatrix BoolMatrix;
    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE,T >::LocalMatrix LocalMatrix;

    //! Create assembler from material parameters and a grid
    OgdenOperatorAssembler(ctype lambda, ctype mu, ctype d, std::shared_ptr<GridFunction> displacement) :
        d_(d),displacement_(displacement)
    {
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }

    //! Create assembler from material parameters
    OgdenOperatorAssembler(ctype lambda, ctype mu, ctype d) :
        d_(d)
    {
        a_= -d_ * Dune::Elasticity::Gamma_x(1);
        b_= (lambda - d_ * (Dune::Elasticity::Gamma_x(1)+Dune::Elasticity::Gamma_xx(1)))/2;
        c_ = mu + d_ * Dune::Elasticity::Gamma_x(1);
    }

    void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        typedef Dune::FieldVector<ctype,dim> FVdim;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

        int rows = tFE.localBasis().size();
        int cols = aFE.localBasis().size();

        localMatrix.setSize(rows,cols);
        localMatrix = 0.0;

        // TODO get proper quadrature rule
        const int order = (element.type().isSimplex())
            ? 4*(tFE.localBasis().order())
            : 4*(tFE.localBasis().order()*dim);
        const Dune::QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<FVdim> gradients(tFE.localBasis().size());

        // store entity geometry mapping
        const typename Element::Geometry geometry = element.geometry();

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const FVdim& quadPos = quad[pt].position();

            // get transposed inverse of Jacobian of transformation
            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradients of base functions
            for (size_t i=0; i<gradients.size(); ++i) {

                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);

            }

            Dune::FieldMatrix<double,dim,dim> nablaPhi[100*dim];
            for (int dof=0; dof<rows; dof++)
                for (int comp=0; comp<dim; comp++)
                    for (int i=0; i<dim; i++)
                        for (int j=0; j<dim; j++)
                            nablaPhi[dof*dim+comp][i][j] = (i==comp) ? gradients[dof][j] : 0;


            // evaluate displacement gradients the configuration at the quadrature point
            typename GridFunction::DerivativeType nablaU;
            if (displacement_->isDefinedOn(element))
                displacement_->evaluateDerivativeLocal(element, quadPos, nablaU);
            else
                displacement_->evaluateDerivative(geometry.global(quadPos), nablaU);


            // compute deformation gradient of the configuration
            for (int i=0;i<dim;i++)
                nablaU[i][i] += 1;

            // evaluate the derminante of the deformation gradient
            const ctype J = nablaU.determinant();
            ctype dGamma = Dune::Elasticity::Gamma_x(J);
            ctype ddGamma = Dune::Elasticity::Gamma_xx(J);

            for (int i=0;i<dim;i++)
                nablaU[i][i] -= 1;

            // assemble strain
            SymmetricTensor<dim,ctype> E;
            Dune::Elasticity::strain(nablaU,E);

            // Compute tr(E)
            ctype trE = E.trace();

            // /////////////////////////////////////////////////
            //   Assemble matrix
            // /////////////////////////////////////////////////
            ctype z = quad[pt].weight() * integrationElement;

            T trE_du;
            compute_trE_du(nablaU,trE_du);

            ctype fuu[dim][dim][dim][dim];

            ogdenHess(nablaU, E, trE, trE_du, dGamma,ddGamma,fuu);

            // loop over all entries of the element stiffness matrix
            // and add the contribution of the current quadrature point
            for (int i=0; i<rows*dim; i++) {

                for (int j=0; j<cols*dim; j++) {

                    // Compute \nabla \phi_i * fuu * \nabla \phi_j

                    // First tensor product:
                    T tmp(0);
                    for (int k=0; k<dim; k++)
                        for (int l=0; l<dim; l++)
                           for (int m=0; m<dim; m++)
                                for (int n=0; n<dim; n++)
                                    tmp[k][l] += fuu[k][l][m][n]*nablaPhi[j][m][n];

                    // Second inner tensor product. Only works for simplices I guess
                    for (int k=0; k<dim; k++)
                        for (int l=0; l<dim; l++)
                            localMatrix[i/dim][j/dim][i%dim][j%dim] += nablaPhi[i][k][l] * tmp[k][l] * z;

                }

            }
        }

    }

    /** \brief Set new configuration to be assembled at. Needed e.g. during Newton iteration.*/
    void setConfiguration(std::shared_ptr<GridFunction> newConfig) {
        displacement_ = newConfig;
    }

private:
    //! The Ogden material parameter
    ctype a_;
    //! The Ogden material parameter
    ctype b_;
    //! The Ogden material parameter
    ctype c_;
    //! The Ogden material parameter
    ctype d_;

    /** \brief The configuration at which the linearized operator is evaluated.*/
    std::shared_ptr<GridFunction> displacement_;

    void compute_trE_du(const Dune::FieldMatrix<ctype,3,3>& u, Dune::FieldMatrix<ctype,3,3>& trE_du) const
    {
        using Dune::Elasticity::E_du;

        trE_du[0][0] = E_du<0,0,0,0>(u) + E_du<1,1,0,0>(u) +E_du<2,2,0,0>(u);
        trE_du[0][1] = E_du<0,0,0,1>(u) + E_du<1,1,0,1>(u) +E_du<2,2,0,1>(u);
        trE_du[0][2] = E_du<0,0,0,2>(u) + E_du<1,1,0,2>(u) +E_du<2,2,0,2>(u);
        trE_du[1][0] = E_du<0,0,1,0>(u) + E_du<1,1,1,0>(u) +E_du<2,2,1,0>(u);
        trE_du[1][1] = E_du<0,0,1,1>(u) + E_du<1,1,1,1>(u) +E_du<2,2,1,1>(u);
        trE_du[1][2] = E_du<0,0,1,2>(u) + E_du<1,1,1,2>(u) +E_du<2,2,1,2>(u);
        trE_du[2][0] = E_du<0,0,2,0>(u) + E_du<1,1,2,0>(u) +E_du<2,2,2,0>(u);
        trE_du[2][1] = E_du<0,0,2,1>(u) + E_du<1,1,2,1>(u) +E_du<2,2,2,1>(u);
        trE_du[2][2] = E_du<0,0,2,2>(u) + E_du<1,1,2,2>(u) +E_du<2,2,2,2>(u);
    }

    void compute_trE_du(const Dune::FieldMatrix<ctype,2,2>& u, Dune::FieldMatrix<ctype,2,2>& trE_du) const
    {
        using Dune::Elasticity::E_du;
        trE_du[0][0] = E_du<0,0,0,0>(u) + E_du<1,1,0,0>(u);
        trE_du[0][1] = E_du<0,0,0,1>(u) + E_du<1,1,0,1>(u);
        trE_du[1][0] = E_du<0,0,1,0>(u) + E_du<1,1,1,0>(u);
        trE_du[1][1] = E_du<0,0,1,1>(u) + E_du<1,1,1,1>(u);
    }

    template <int p, int q, int r, int s>
    ctype compute_fuu_1(const Dune::FieldMatrix<ctype,3,3>& u,
                        const SymmetricTensor<3,ctype>& E, ctype trE, const Dune::FieldMatrix<ctype,3,3> trE_du,
                        ctype dGamma, ctype ddGamma) const {

        ctype trE_dupqrs = Dune::Elasticity::E_dudu<0,0,p,q,r,s>()+Dune::Elasticity::E_dudu<1,1,p,q,r,s>()+Dune::Elasticity::E_dudu<2,2,p,q,r,s>();

        ctype result = a_*trE_dupqrs + 2*b_*( trE*trE_dupqrs + trE_du[r][s]*trE_du[p][q] );

        ctype tmp = 0;
        tmp += Dune::Elasticity::E_du<0,0,r,s>(u)*Dune::Elasticity::E_du<0,0,p,q>(u) + E(0,0)*Dune::Elasticity::E_dudu<0,0,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<0,1,r,s>(u)*Dune::Elasticity::E_du<0,1,p,q>(u) + E(0,1)*Dune::Elasticity::E_dudu<0,1,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<0,2,r,s>(u)*Dune::Elasticity::E_du<0,2,p,q>(u) + E(0,2)*Dune::Elasticity::E_dudu<0,2,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<1,0,r,s>(u)*Dune::Elasticity::E_du<1,0,p,q>(u) + E(1,0)*Dune::Elasticity::E_dudu<1,0,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<1,1,r,s>(u)*Dune::Elasticity::E_du<1,1,p,q>(u) + E(1,1)*Dune::Elasticity::E_dudu<1,1,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<1,2,r,s>(u)*Dune::Elasticity::E_du<1,2,p,q>(u) + E(1,2)*Dune::Elasticity::E_dudu<1,2,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<2,0,r,s>(u)*Dune::Elasticity::E_du<2,0,p,q>(u) + E(2,0)*Dune::Elasticity::E_dudu<2,0,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<2,1,r,s>(u)*Dune::Elasticity::E_du<2,1,p,q>(u) + E(2,1)*Dune::Elasticity::E_dudu<2,1,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<2,2,r,s>(u)*Dune::Elasticity::E_du<2,2,p,q>(u) + E(2,2)*Dune::Elasticity::E_dudu<2,2,p,q,r,s>();

        result += 2*c_*tmp + d_*( dGamma*Dune::Elasticity::det_dudu_tmpl<p,q,r,s>(u)
                                + ddGamma*Dune::Elasticity::det_du_tmpl<p,q>(u)*Dune::Elasticity::det_du_tmpl<r,s>(u) );
        // precomputating and storing det_du(r,s) seems to yield exactly the same performance...


        return result;
    }

    template <int p, int q, int r, int s>
    ctype compute_fuu_1(const Dune::FieldMatrix<ctype,2,2>& u,
                                const SymmetricTensor<2,ctype>& E, ctype trE, const Dune::FieldMatrix<ctype,2,2> trE_du,
                                ctype dGamma, ctype ddGamma) const {
        ctype trE_dupqrs = Dune::Elasticity::E_dudu<0,0,p,q,r,s>()+Dune::Elasticity::E_dudu<1,1,p,q,r,s>();

        ctype result = a_*trE_dupqrs + 2*b_*( trE*trE_dupqrs + trE_du[r][s]*trE_du[p][q] );

        ctype tmp = 0;
        tmp += Dune::Elasticity::E_du<0,0,r,s>(u)*Dune::Elasticity::E_du<0,0,p,q>(u) + E(0,0)*Dune::Elasticity::E_dudu<0,0,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<0,1,r,s>(u)*Dune::Elasticity::E_du<0,1,p,q>(u) + E(0,1)*Dune::Elasticity::E_dudu<0,1,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<1,0,r,s>(u)*Dune::Elasticity::E_du<1,0,p,q>(u) + E(1,0)*Dune::Elasticity::E_dudu<1,0,p,q,r,s>();
        tmp += Dune::Elasticity::E_du<1,1,r,s>(u)*Dune::Elasticity::E_du<1,1,p,q>(u) + E(1,1)*Dune::Elasticity::E_dudu<1,1,p,q,r,s>();

        result += 2*c_*tmp + d_*( dGamma*Dune::Elasticity::det_dudu_tmpl<p,q,r,s>(u)
                                + ddGamma*Dune::Elasticity::det_du_tmpl<p,q>(u)*Dune::Elasticity::det_du_tmpl<r,s>(u) );
        // precomputating and storing det_du(r,s) seems to yield exactly the same performance...

        return result;
    }

    template <int p, int q>
    void compute_fuu(const Dune::FieldMatrix<ctype,3,3>& u,
                            const SymmetricTensor<3,ctype>& E, ctype trE, const Dune::FieldMatrix<ctype,3,3>& trE_du,
                            ctype dGamma, ctype ddGamma,
                            ctype fuu[3][3]) const {
        fuu[0][0] = compute_fuu_1<p,q,0,0>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[0][1] = compute_fuu_1<p,q,0,1>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[0][2] = compute_fuu_1<p,q,0,2>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[1][0] = compute_fuu_1<p,q,1,0>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[1][1] = compute_fuu_1<p,q,1,1>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[1][2] = compute_fuu_1<p,q,1,2>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[2][0] = compute_fuu_1<p,q,2,0>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[2][1] = compute_fuu_1<p,q,2,1>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[2][2] = compute_fuu_1<p,q,2,2>(u,E,trE,trE_du,dGamma,ddGamma);
    }

    template <int p, int q>
    void compute_fuu(const Dune::FieldMatrix<ctype,2,2>& u,
                            const SymmetricTensor<2,ctype>& E, ctype trE, const Dune::FieldMatrix<ctype,2,2>& trE_du,
                            ctype dGamma, ctype ddGamma,
                            ctype fuu[2][2]) const {
        fuu[0][0] = compute_fuu_1<p,q,0,0>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[0][1] = compute_fuu_1<p,q,0,1>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[1][0] = compute_fuu_1<p,q,1,0>(u,E,trE,trE_du,dGamma,ddGamma);
        fuu[1][1] = compute_fuu_1<p,q,1,1>(u,E,trE,trE_du,dGamma,ddGamma);
    }


    void ogdenHess(const Dune::FieldMatrix<ctype, 3,3>& u, const SymmetricTensor<3,ctype>& E,
                   ctype trE, const Dune::FieldMatrix<ctype,3,3>& trE_du,
                   const ctype& dGamma, const ctype& ddGamma,
                                ctype fuu[3][3][3][3]) const {
        // Collect results
        compute_fuu<0,0>(u,E,trE,trE_du,dGamma,ddGamma,fuu[0][0]);
        compute_fuu<0,1>(u,E,trE,trE_du,dGamma,ddGamma,fuu[0][1]);
        compute_fuu<0,2>(u,E,trE,trE_du,dGamma,ddGamma,fuu[0][2]);
        compute_fuu<1,0>(u,E,trE,trE_du,dGamma,ddGamma,fuu[1][0]);
        compute_fuu<1,1>(u,E,trE,trE_du,dGamma,ddGamma,fuu[1][1]);
        compute_fuu<1,2>(u,E,trE,trE_du,dGamma,ddGamma,fuu[1][2]);
        compute_fuu<2,0>(u,E,trE,trE_du,dGamma,ddGamma,fuu[2][0]);
        compute_fuu<2,1>(u,E,trE,trE_du,dGamma,ddGamma,fuu[2][1]);
        compute_fuu<2,2>(u,E,trE,trE_du,dGamma,ddGamma,fuu[2][2]);
    }

    void ogdenHess(const Dune::FieldMatrix<ctype, 2,2>& u, const SymmetricTensor<2,ctype>& E,
                   ctype trE, const Dune::FieldMatrix<ctype,2,2>& trE_du,
                   const ctype& dGamma, const ctype& ddGamma,
                                ctype fuu[2][2][2][2]) const {

        // Collect results
        compute_fuu<0,0>(u,E,trE,trE_du,dGamma,ddGamma,fuu[0][0]);
        compute_fuu<0,1>(u,E,trE,trE_du,dGamma,ddGamma,fuu[0][1]);
        compute_fuu<1,0>(u,E,trE,trE_du,dGamma,ddGamma,fuu[1][0]);
        compute_fuu<1,1>(u,E,trE,trE_du,dGamma,ddGamma,fuu[1][1]);
    }
};


#endif

