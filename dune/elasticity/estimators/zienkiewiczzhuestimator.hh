#ifndef ZIENKIEWICZ_ZHU_ESTIMATOR_HH
#define ZIENKIEWICZ_ZHU_ESTIMATOR_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bdmatrix.hh>
#include <dune/istl/solvers.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/lumpmatrix.hh>

//#define LUMP_MATRIX

template <class GridType, class VectorType>
class ZienkiewiczZhuEstimator
{

    typedef typename GridType::template Codim<0>::LeafIterator LeafElementIterator;

    enum {dim = GridType::dimension};

public:

    ZienkiewiczZhuEstimator(const GridType& grid, double E, double nu)
        : grid_(&grid), E_(E), nu_(nu)
    {}

    void estimate(const VectorType& x, 
                  Dune::BlockVector<Dune::FieldVector<double,1> >& errorPerElement) {

        using namespace Dune;
        
        const auto& indexSet = grid_->leafIndexSet();


        errorPerElement.resize(indexSet.size(0));

        // ////////////////////////////////////////////////////////////////////////
        //   Create mass matrix
        // ////////////////////////////////////////////////////////////////////////

        //Blocktype is of size of the internal of a symmetric tensor, that is dim*(dim+1)/2
        typedef FieldMatrix<double,dim*(dim+1)/2, dim*(dim+1)/2> BlockType;
        typedef BCRSMatrix<BlockType> MassMatrixType;
        typedef P1NodalBasis<typename GridType::LeafGridView,double> P1Basis;

                
                
        P1Basis p1Basis(grid_->leafGridView());
        MassMatrixType massMatrix;
        OperatorAssembler<P1Basis,P1Basis> globalAssembler(p1Basis,p1Basis);
        MassAssembler<GridType, typename P1Basis::LocalFiniteElement, typename P1Basis::LocalFiniteElement,BlockType> localMassAssembler;
        globalAssembler.assemble(localMassAssembler,massMatrix);

        // /////////////////////////////////////////////////////////////
        //   Lump and invert mass matrix
        // /////////////////////////////////////////////////////////////
#ifdef LUMP_MATRIX
       /* BDMatrix<FieldMatrix<double,1,1> > invMassMatrix(baseSet.size());
        invMassMatrix = 0;
        
        for (int i=0; i<baseSet.size(); i++) {
            
            for (int j=0; j<baseSet.size(); j++)
                invMassMatrix[i][i] += massMatrix[i][j];
            
        }
        */
        BDMatrix<BlockType> invMassMatrix;
        invMassMatrix = 0;
                
        lumpMatrix(massMatrix,invMassMatrix);
        
        invMassMatrix.invert();
#endif     
       
        BlockVector<SymmetricTensor<dim> > unscaledP1Stress(indexSet.size(dim));
        unscaledP1Stress  = 0;

        typedef Dune::PQkLocalFiniteElementCache<typename GridType::ctype, double, dim, 1> FiniteElementCache;
        FiniteElementCache cache; 
        
        for (const auto& e : elements(grid_->leafGridView())) {

            // Element type
            GeometryType gt = e.geometry().type();

            // First order Lagrange shape functions
            const typename FiniteElementCache::FiniteElementType& finiteElement = cache.get(gt);

            // /////////////////////////////////////////
            //   Extract the solution on this element
            // /////////////////////////////////////////

            VectorType localSolution(finiteElement.localBasis().size());

            for (size_t i=0; i<finiteElement.localBasis().size(); i++)
                localSolution[i] = x[indexSet.subIndex(e,i,dim)];

            /** \todo Determine correct quadrature order */
            int quadOrder = (gt.isSimplex()) ? 2 : dim;
            const QuadratureRule<double,dim>& quad = QuadratureRules<double,dim>::rule(gt,quadOrder);

            // ///////////////////////////////////////////////////////////
            //   Compute P1-projection of stress
            // ///////////////////////////////////////////////////////////

            auto&& geometry =  e.geometry();

            for (size_t g=0; g<quad.size(); ++g) {
              
                // pos of integration point
                const FieldVector<double,dim>& quadPos = quad[g].position();
                
                // eval jacobian inverse
                auto&& jac = geometry.jacobianInverseTransposed(quadPos);

                // determinant of jacobian times quadrature weight
                auto factor = geometry.integrationElement(quadPos) * quad[g].weight();

                // /////////////////////////////////////////////
                //   Compute p0 stress at this quadrature node
                // /////////////////////////////////////////////
                SymmetricTensor<dim> p0Strain(0.0);
                
                // evaluate gradients at Gauss points
                std::vector<FieldMatrix<double,1,dim> > temp;
                FieldVector<double,dim> grad;
                
                finiteElement.localBasis().evaluateJacobian(quadPos,temp);
                
                for (size_t i=0; i<finiteElement.localBasis().size(); i++) {
                	
                	grad = 0;
                	jac.umv(temp[i][0],grad); // transform gradient to global coordinates
                        
                    for (int j=0; j<dim; j++) {
                        
                        // The deformation gradient of the shape function
                        FieldMatrix<double, dim, dim> Grad(0);
                        Grad[j] = grad;
                        
                        /* Computes the linear strain tensor from the deformation gradient*/
                        SymmetricTensor<dim> scaledStrain;
                        computeStrain(Grad,scaledStrain);
                        
                        scaledStrain *= localSolution[i][j];
                        p0Strain += scaledStrain;

                    }
                    
                }

                // compute stress
                SymmetricTensor<dim> p0Stress = hookeTimesStrain(p0Strain);
                
                std::vector<FieldVector<double,1> >values;
                finiteElement.localBasis().evaluateFunction(quadPos,values);

                // loop over test function number
                for (size_t row=0; row<finiteElement.localBasis().size(); row++) {

                    int globalRow = indexSet.subIndex(e,row,dim);
                    
                    for (size_t rcomp=0; rcomp<p0Stress.size(); rcomp++) {
             
                        unscaledP1Stress[globalRow][rcomp] += p0Stress[rcomp]*values[row] * factor;

                    }    

                }
              
            }

        }

        // /////////////////////////////////////////////////////////////
        //
        // /////////////////////////////////////////////////////////////

        BlockVector<SymmetricTensor<dim> > elementP1Stress(indexSet.size(dim));
        elementP1Stress  = 0;
        
#ifdef LUMP_MATRIX
        /*elementP1Stress = unscaledP1Stress;
        for (int i=0; i<baseSet.size(); i++)
            elementP1Stress *= invMassMatrix[i][i];
        */
        BlockVector<SymmetricTensor<dim> > tmp(indexSet.size(dim));
        tmp = unscaledP1Stress;
        invMassMatrix.mv(tmp,elementP1Stress);
#else
            
        // Technicality:  turn the matrix into a linear operator
        MatrixAdapter<MassMatrixType, BlockVector<SymmetricTensor<dim> >, BlockVector<SymmetricTensor<dim> > > op(massMatrix);
        
        // A preconditioner
        SeqILU<MassMatrixType, BlockVector<SymmetricTensor<dim> >, BlockVector<SymmetricTensor<dim> > > ilu0(massMatrix,1.0);
        
        // A preconditioned conjugate-gradient solver
        int numIt = 100;
        CGSolver<BlockVector<SymmetricTensor<dim> > > cg(op,ilu0,1E-16,numIt,0); 
        
        // Object storing some statistics about the solving process
        InverseOperatorResult statistics;
        
        // Solve!
        cg.apply(elementP1Stress, unscaledP1Stress, statistics);
#endif

        for (const auto& e : elements(grid_->leafGridView())) {

            // Element type
            GeometryType gt = e.type();

            // First order finite element
            const typename FiniteElementCache::FiniteElementType& finiteElement = cache.get(gt); 

            // /////////////////////////////////////////
            //   Extract the solution on this element
            // /////////////////////////////////////////

            VectorType localSolution(finiteElement.localBasis().size());

            for (size_t i=0; i<finiteElement.localBasis().size(); i++)
                localSolution[i] = x[indexSet.subIndex(e,i,dim)];

            /** \todo Determine correct quadrature order */
            int quadOrder = (gt.isSimplex()) ? 2 : dim;
            const QuadratureRule<double,dim>& quad = QuadratureRules<double,dim>::rule(gt,quadOrder);

            // ////////////////////////////////////////////////////////////////
            //   Compute the stress error for this element
            // ////////////////////////////////////////////////////////////////

            errorPerElement[indexSet.index(e)] = 0;

            for (size_t g=0; g<quad.size(); ++g) {
              
                // pos of integration point
                const auto& quadPos = quad[g].position();
                
                // eval jacobian inverse
                const auto& jac = e.geometry().jacobianInverseTransposed(quadPos);

                // weight of quadrature point
                double weight = quad[g].weight();
              
                // determinant of jacobian
                double detjac = e.geometry().integrationElement(quadPos);

                // Stresses at this quadrature point
                SymmetricTensor<dim> p0Stress(0.0), p1Stress(0.0);
                
                // evaluate gradients at Gauss points
                std::vector<FieldMatrix<double,1,dim> >temp;
                std::vector<FieldVector<double,1> >values;
                FieldVector<double,dim> grad;

                finiteElement.localBasis().evaluateJacobian(quadPos,temp);
                finiteElement.localBasis().evaluateFunction(quadPos,values);

                // loop over test function number
                for (size_t i=0; i<finiteElement.localBasis().size(); i++) {
                    
                    grad = 0;
                    jac.umv(temp[i][0],grad); // transform gradient to global coordinates
                        
                    for (int j=0; j<dim; j++) {
                        
                        // Compute strain of shape functions
                        FieldMatrix<double, dim, dim> Grad(0);
                        Grad[j] = grad;
                 
                        /* Computes the linear strain tensor from the deformation gradient*/
                        SymmetricTensor<dim> shapeFunctionStrain;
                        computeStrain(Grad,shapeFunctionStrain);

                        shapeFunctionStrain *= localSolution[i][j];

                        // Compute stress
                        p0Stress += hookeTimesStrain(shapeFunctionStrain);
                    }    

                    SymmetricTensor<dim> scaledStress = elementP1Stress[i];
                    scaledStress *= values[i];
                    p1Stress += scaledStress;
                }

                // Integrate error
                errorPerElement[indexSet.index(e)] += (p0Stress-p1Stress).two_norm() * weight * detjac;

            }
        }
    }

private:
		  
	void computeStrain(const Dune::FieldMatrix<double, dim, dim>& grad, SymmetricTensor<dim>& strain) const
	    {
		for (int i=0; i<dim ; ++i)
		{
			strain(i,i) = grad[i][i];
			for (int j=i+1; j<dim; ++j)
				strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
		}
	        
	    }

	SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {
	          
		SymmetricTensor<dim> stress = strain;
		stress *= E_/(1+nu_);

		// Compute the trace of the strain
		double trace = 0;
		for (int i=0; i<dim; i++)
			trace += strain[i];

		double f = E_*nu_ / ((1+nu_)*(1-2*nu_)) * trace;

		for (int i=0; i<dim; i++)
			stress[i] += f;

		return stress;
	}

    // /////////////////////////////////
    //   Data members
    // /////////////////////////////////

    const GridType* grid_;

    double E_;

    double nu_;

};

#endif
