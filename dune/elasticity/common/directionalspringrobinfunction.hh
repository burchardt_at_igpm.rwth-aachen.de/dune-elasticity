// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ELASTICITY_UTILITIES_DIRECTIONAL_SPRING_ROBIN_FUNCTION_HH
#define DUNE_ELASTICITY_UTILITIES_DIRECTIONAL_SPRING_ROBIN_FUNCTION_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/typetraits.hh>

#include <dune/fufem/functions/basisgridfunction.hh>

/** \brief A matrix valued basis grid function that returns the scaled dyadic product of a given vector field
 */
template <class Basis, class CoefficientVector>
class DirectionalSpringRobinFunction :
  public VirtualGridViewFunction<typename Basis::GridView, Dune::FieldMatrix<Dune::field_t<CoefficientVector>,
                                                      Basis::GridView::dimension,
                                                      Basis::GridView::dimension> >
{
  using field_type = Dune::field_t<CoefficientVector>;
  static constexpr size_t dim = Basis::GridView::dimension;
  using Matrix = Dune::FieldMatrix<field_type, dim, dim>;

  using Base = VirtualGridViewFunction<typename Basis::GridView, Matrix>;
public:

  using typename Base::LocalDomainType;
  using typename Base::DomainType;
  using typename Base::RangeType;
  using typename Base::Element;

  DirectionalSpringRobinFunction(const Basis& basis, const CoefficientVector& directionField, field_type scaling = 1.0) :
    Base(basis.getGridView()),
    directionField_(basis, directionField),
    scaling_(scaling)
  {}

  void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const override
  {
    typename CoefficientVector::block_type direction;
    directionField_.evaluateLocal(e, x, direction);
    direction /= direction.two_norm();

    y = 0;
    for (size_t i=0; i<dim; ++i)
      y[i].axpy(scaling_*direction[i], direction);
  }

private:
  //! Gridfunction containing the direction field
  BasisGridFunction<Basis, CoefficientVector> directionField_;

  //! A scaling factor for the dyadic matrices
  field_type scaling_;
};

#endif
