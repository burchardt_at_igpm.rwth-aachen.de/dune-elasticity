// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_MATERIALS_HYPERELASTIC_MATERIAL_HH
#define DUNE_ELASTICITY_MATERIALS_HYPERELASTIC_MATERIAL_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>

#include <dune/solvers/common/wrapownshare.hh>

/** \brief Base class for hyperelastic materials. */
template <class Basis>
class Material
{
    public:
        typedef Basis GlobalBasis;
        typedef typename Basis::GridView::Grid GridType;
        typedef typename Basis::LocalFiniteElement Lfe;
        typedef typename Basis::ReturnType ReturnType;

        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef Dune::FieldVector<ReturnType,dim> block_type;
        typedef Dune::BlockVector<block_type> VectorType;
        typedef LocalFunctionalAssembler<GridType,Lfe, block_type > LocalLinearization;
        typedef LocalOperatorAssembler<GridType,Lfe,Lfe,Dune::FieldMatrix<ReturnType,dim,dim> > LocalHessian;
        typedef VirtualGridFunction<GridType, block_type> GridFunction;

    public:
        Material() = default;

        template <class BasisT>
        Material(BasisT&& basis) :
          basis_(Dune::Solvers::wrap_own_share<const Basis>(std::forward<BasisT>(basis)))
        {}

        //! Evaluate the strain energy
        virtual ReturnType energy(std::shared_ptr<GridFunction> displace) const = 0;

        //! Return the local assembler of the first derivative of the strain energy at some displacement
        virtual LocalLinearization& firstDerivative(std::shared_ptr<GridFunction> displace) = 0;

        //! Return the local assembler of the second derivative of the strain energy at some displacement
        virtual LocalHessian& secondDerivative(std::shared_ptr<GridFunction> displace) = 0;

        //! Return the global basis
        const Basis& basis() const {return *basis_;}

        template <class BasisT>
        void setBasis(BasisT&& basis) {
          basis_ = Dune::Solvers::wrap_own_share<const Basis>(std::forward<BasisT>(basis));
        }

    protected:
        //! Global basis used for the spatial discretization
        std::shared_ptr<const Basis> basis_;
};

#endif
