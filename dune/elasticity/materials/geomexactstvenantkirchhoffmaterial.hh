// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_MATERIALS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_MATERIAL_HH
#define DUNE_ELASTICITY_MATERIALS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_MATERIAL_HH

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/assemblers/localassemblers/geomexactstvenantkirchhofffunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/geomexactstvenantkirchhoffoperatorassembler.hh>

#include <dune/elasticity/materials/material.hh>
#include <dune/elasticity/common/elasticityhelpers.hh>

/* \brief Class representing a hyperelastic geometrically exact St.Venant Kirchhoff material.
 *
 * \tparam Basis    Global basis that is used for the spatial discretization. 
 *                  (This is not nice but I need a LocalFiniteElement for the local Hessian assembler :-( )
*/
template <class Basis>
class GeomExactStVenantMaterial : public Material<Basis>
{
    typedef Material<Basis> Base;
public:
    typedef typename Basis::GridView::Grid GridType;
    typedef typename Base::GlobalBasis GlobalBasis;
    typedef typename Base::Lfe Lfe;
    typedef typename Base::LocalLinearization LocalLinearization;
    typedef typename Base::LocalHessian LocalHessian;
    typedef typename Base::GridFunction GridFunction;
    typedef typename Base::ReturnType ReturnType;


private:
    using Base::dim;
    typedef typename GridType::ctype ctype;
    typedef GeomExactStVenantKirchhoffFunctionalAssembler<GridType,Lfe> LinearisationAssembler;
    typedef GeomExactStVenantKirchhoffOperatorAssembler<GridType, Lfe, Lfe> HessianAssembler;

    using Derivative = typename GridFunction::DerivativeType;
    using StressFunction = std::function<SymmetricTensor<dim, ReturnType>(Derivative)>;

public:
    GeomExactStVenantMaterial() :
        localLinearisation_(-1, -1),
        localHessian_(-1, -1),
        lambda_(1.0),
        mu_(0.3)
    {}

    template <class BasisT>
    GeomExactStVenantMaterial(BasisT&& basis, ReturnType E, ReturnType nu) :
        Base(std::forward<BasisT>(basis)),
        localLinearisation_(E, nu),
        localHessian_(E, nu),
        lambda_(E*nu/((1+nu)*(1-2*nu))),
        mu_(E/(2+2*nu))
    {}

    template <class BasisT>
    void setup(BasisT&& basis, ReturnType E, ReturnType nu)
    {

        lambda_ = E*nu/((1+nu)*(1-2*nu));
        mu_ = E/(2+2*nu);

        localLinearisation_ = LinearisationAssembler(E, nu);
        localHessian_ = HessianAssembler(E, nu);

        this->setBasis(std::forward<BasisT>(basis));
    }

    //! Evaluate the strain energy
    ReturnType energy(std::shared_ptr<GridFunction> displace) const
    {
        ReturnType energy{0};

        const auto& gridView = this->basis_->getGridView();

        for (const auto& e : elements(gridView)) {

            // get quadrature rule
            QuadratureRuleKey quad1(this->basis_->getLocalFiniteElement(e));
            QuadratureRuleKey quadKey = quad1.derivative().square().square();

            const auto& quad = QuadratureRuleCache<ctype, dim>::rule(quadKey);

            const auto& geometry = e.geometry();

            for (const auto& pt : quad) {

                const auto& quadPos = pt.position();

                Derivative localDispGrad;
                if (displace->isDefinedOn(e))
                    displace->evaluateDerivativeLocal(e, quadPos, localDispGrad);
                else
                    displace->evaluateDerivative(geometry.global(quadPos), localDispGrad);

                SymmetricTensor<dim, ReturnType> strain;
                Dune::Elasticity::strain(localDispGrad, strain);

                SymmetricTensor<dim, ReturnType> stress = strain;
                stress *= 2*mu_;
                stress.addToDiag(lambda_ * strain.trace());

                ReturnType z = pt.weight() * geometry.integrationElement(quadPos);
                energy += (stress*strain)*z;
            }
        }
        return 0.5*energy;
    }

    //! Return the local assembler of the first derivative of the strain energy 
    LinearisationAssembler& firstDerivative(std::shared_ptr<GridFunction> displace)
    {
        localLinearisation_.setConfiguration(displace);
        return localLinearisation_;
    }

    //! Return the local assembler of the second derivative of the strain energy 
    HessianAssembler& secondDerivative(std::shared_ptr<GridFunction> displace)
    {
        localHessian_.setConfiguration(displace);
        return localHessian_;
    }

    static StressFunction stressFunction(ReturnType E, ReturnType nu) {
      ReturnType lambda{E * nu / (1.0+nu) / (1.0-2.0*nu)};
      ReturnType twoMu{E / (1+nu)};

      return [=](const Derivative& dispGrad) {

          SymmetricTensor<dim,ReturnType> strain;
          Dune::Elasticity::strain(dispGrad, strain);

          SymmetricTensor<dim, ReturnType> stress = strain;
          stress *= twoMu;
          stress.addToDiag(lambda * strain.trace());

          return stress;
      };
    }

private:
    //! First derivative of the strain energy
    LinearisationAssembler localLinearisation_;

    //! Second derivative of the strain energy
    HessianAssembler localHessian_;

    //! 1. Lam\'e constant
    ReturnType lambda_;
    //! 2. Lam\'e constant
    ReturnType mu_;
};

#endif
