// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_ELASTICITY_MATERIALS_ADOLC_MATERIAL_HH
#define DUNE_ELASTICITY_MATERIALS_ADOLC_MATERIAL_HH

#if HAVE_ADOLC
#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>
#include <dune/fufem/assemblers/localassemblers/adolclinearizationassembler.hh>
#include <dune/fufem/assemblers/localassemblers/adolchessianassembler.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/elasticity/materials/material.hh>

/** \brief Class representing a possibly nonlinear hyperelastic material. The linearization and hessian
 *         are computed using the automatic differentiation library ADOL-C.
 *
 * \tparam Basis    Global basis that is used for the spatial discretization.
 *                  (This is not nice but I need a LocalFiniteElement for the local Hessian assembler :-( )
 */
template <class Basis>
class AdolcMaterial : public Material<Basis>
{
public:
    typedef Material<Basis> Base;
    typedef typename Base::GridType GridType;
    typedef typename Base::GlobalBasis GlobalBasis;
    typedef typename Base::Lfe Lfe;
    typedef typename Base::LocalLinearization LocalLinearization;
    typedef typename Base::LocalHessian LocalHessian;
    typedef typename Base::VectorType VectorType;
    typedef typename Base::GridFunction GridFunction;
    typedef typename Base::ReturnType ReturnType;

    using Base::dim;
    typedef Adolc::LocalEnergy<GridType, Lfe, dim> LocalEnergy;

private:

    typedef typename Dune::LocalFiniteElementFunctionBase<Lfe>::type FunctionBaseClass;
    typedef LocalFunctionComponentWrapper<GridFunction, GridType, FunctionBaseClass> LocalWrapper;

    typedef typename VectorType::block_type FVector;
    typedef typename FVector::field_type field_type;
    typedef Dune::FieldMatrix<field_type,dim,dim> FMatrix;

    typedef typename GridType::ctype ctype;
    typedef AdolcLinearizationAssembler<GridType,Lfe,FVector> AdolcLinearization;
    typedef AdolcHessianAssembler<GridType,Lfe,Lfe,FMatrix> AdolcHessian;

public:
    AdolcMaterial() {}

    template <class BasisT>
    AdolcMaterial(BasisT&& basis, const LocalEnergy& localEnergy, bool vectorMode=true) :
        Base(std::forward<BasisT>(basis)),
        localEnergy_(&localEnergy),
        localLinearization_(localEnergy),
        localHessian_(localEnergy,vectorMode)
    {}

    template <class BasisT>
    void setup(BasisT&& basis, const LocalEnergy& localEnergy, bool vectorMode=true)
    {
        localEnergy_ = &localEnergy;
        localLinearization_ =  AdolcLinearization(localEnergy);
        localHessian_ = AdolcHessian(localEnergy,vectorMode);

        this->setBasis(std::forward<BasisT>(basis));
    }

    //! Evaluate the strain energy
    ReturnType energy(std::shared_ptr<GridFunction> displace) const
    {
        typename LocalEnergy::ReturnType aEnergy(0.0);

        for (const auto& e : elements(this->basis().getGridView())) {

            const auto& lfe = this->basis().getLocalFiniteElement(e);

            // interpolate by local finite element to get the local coefficients
            typename LocalEnergy::CoefficientVectorType localCoeff(lfe.localBasis().size());
            LocalWrapper fiLocal(*displace, e, 0);

            std::vector<typename LocalWrapper::RangeType> interpolationValues;
            for (int i=0; i < dim; i++) {
                fiLocal.setIndex(i);

                lfe.localInterpolation().interpolate(fiLocal, interpolationValues);
                for (size_t j=0; j< lfe.localBasis().size(); j++)
                    localCoeff[j][i] = interpolationValues[j];
            }

            aEnergy += localEnergy_->energy(e,lfe, localCoeff);
        }

        ReturnType energy;
        aEnergy >>= energy;

        return energy;
    }

    //! Return the local assembler of the first derivative of the strain energy
    LocalLinearization& firstDerivative(std::shared_ptr<GridFunction> displace) {

        localLinearization_.setConfiguration(displace);
        return localLinearization_;
    }

    //! Return the local assembler of the second derivative of the strain energy
    LocalHessian& secondDerivative(std::shared_ptr<GridFunction> displace) {

        localHessian_.setConfiguration(displace);
        return localHessian_;
    }

private:
    //! Local energy
    const LocalEnergy* localEnergy_;

    //! Local linearization
    AdolcLinearization localLinearization_;

    //! Local hessian
    AdolcHessian localHessian_;
};
#endif // HAVE_ADOLC
#endif
