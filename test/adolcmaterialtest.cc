#include<config.h>

#include <fstream>
#include <random>

#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/timer.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/grid/uggrid.hh>

#include <dune/fufem/makesphere.hh>
#include <dune/fufem/makering.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/operatorassembler.hh>
#include <dune/fufem/assemblers/functionalassembler.hh>

#define LAURSEN

#include <dune/elasticity/materials/adolcmaterial.hh>
#include <dune/elasticity/materials/neohookeanmaterial.hh>
#include <dune/elasticity/materials/mooneyrivlinmaterial.hh>
#include <dune/elasticity/materials/geomexactstvenantkirchhoffmaterial.hh>


// grid dimension
const int dim = 3;

using namespace Dune;
int main (int argc, char *argv[]) try
{
    Dune::MPIHelper::instance(argc, argv);

    using FVector = FieldVector<double, dim>;

    //    Create the grid
    using Grid = UGGrid<dim>;
    auto grid = makeSphere<Grid>(FVector(0), 0.2);
    grid->globalRefine(3);

    using FEBasis = P1NodalBasis<typename Grid::LeafGridView>;
    FEBasis feBasis(grid->leafGridView());

    //   Initial iterate
    double lower_bound = 0;
    double upper_bound = 0.05;
    std::uniform_real_distribution<double> unif(lower_bound, upper_bound);
    std::default_random_engine re;

    using BVector = BlockVector<FVector>;
    BVector x(feBasis.size());
    for (size_t i = 0; i < x.size(); ++i)
        for (int j = 0; j < dim; ++j)
            x[i][j] = unif(re);
    auto xGridFunc = std::make_shared<BasisGridFunction<FEBasis,BVector> >(feBasis,x);

    // make a material
    using Material = NeoHookeanMaterial<FEBasis>;
    Material material(feBasis, 1e5, 0.4);

    // check against the Adol-C material
    AdolcMaterial<FEBasis> adolcMaterial(feBasis, material, false);

    auto& linPaperAssembler = material.firstDerivative(xGridFunc);
    auto& linAdolcAssembler = adolcMaterial.firstDerivative(xGridFunc);

    FunctionalAssembler<FEBasis> functionalAssembler(feBasis);

    BVector adolcGradient(feBasis.size());
    BVector paperGrad(feBasis.size());

    adolcGradient = 0;
    paperGrad = 0;

    Timer time;
    functionalAssembler.assemble(linAdolcAssembler, adolcGradient);
    std::cout << "ADOL-C functional assembler needed " << time.stop() << std::endl;

    time.reset();
    time.start();
    functionalAssembler.assemble(linPaperAssembler, paperGrad);
    std::cout << "Paper&Pen functional assembler needed " << time.stop() << std::endl;

    for (size_t i=0; i < adolcGradient.size(); ++i) {

        auto diff = adolcGradient[i] - paperGrad[i];

        if (diff.two_norm()>1e-5)
            DUNE_THROW(Dune::Exception,"Wrong local derivative, error is "<<diff.two_norm());
    }

    // Test hessian assembler
    auto& hessPaperAssembler = material.secondDerivative(xGridFunc);
    auto& hessAdolcAssembler = adolcMaterial.secondDerivative(xGridFunc);

    OperatorAssembler<FEBasis, FEBasis> operatorAssembler(feBasis, feBasis);

    using Matrix = BCRSMatrix<FieldMatrix<double, dim, dim> >;
    Matrix adolcHessian, paperHessian;

    time.reset();
    time.start();
    operatorAssembler.assemble(hessAdolcAssembler, adolcHessian);
    std::cout << "ADOL-C operator assembler needed " << time.stop() << std::endl;

    time.reset();
    time.start();

    operatorAssembler.assemble(hessPaperAssembler, paperHessian);
    std::cout << "Paper&Pen operator assembler needed " << time.stop() << std::endl;

    for (size_t i=0; i < adolcHessian.N(); ++i) {

        const auto& adolcRow = adolcHessian[i];
        const auto& paperRow = paperHessian[i];

        auto adolcIt = adolcRow.begin();
        auto adolcEndIt = adolcRow.end();

        auto paperIt = paperRow.begin();
        auto paperEndIt = paperRow.end();

        for (; adolcIt != adolcEndIt; ++adolcIt, ++paperIt) {

            if (adolcIt.index() != paperIt.index())
                DUNE_THROW(Dune::Exception,"Not the same sparsity pattern!"<<adolcIt.index()
                                <<"!="<<paperIt.index());

            auto diff = *adolcIt;
            diff -= *paperIt;

            if (diff.frobenius_norm() > 1e-4)
                DUNE_THROW(Dune::Exception,"Wrong local hessian, error is "<<diff.frobenius_norm());
        }
        assert(paperIt==paperEndIt);
    }

    return 0;

} catch (Exception e) {
    std::cout << e << std::endl;
    return 1;
}
