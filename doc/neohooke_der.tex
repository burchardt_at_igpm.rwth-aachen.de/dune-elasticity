\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{psfrag}

\graphicspath{{gfx/}}

\providecommand{\parder}[2]{\frac{\partial #1}{\partial #2}}
\providecommand{\secparder}[3]{\frac{\partial {#1}^2}{\partial #2 \partial #3}}
\newcommand{\tr}{\operatorname{tr}}

\begin{document}

\section{The Derivatives of the Energy Functional of a Neo-Hookean Material}

\subsection{Preliminaries: The Derivative of the Determinant of the Deformation  Gradient}

Let
\begin{equation*}
  J = \det F = \det \nabla (X + u) = \det (I + \nabla u) 
  = \det (I + \frac{\partial u_i}{\partial X^j}).
\end{equation*}
The deformation $u$ is a finite element function $u = \sum_{i,j} u_i^j \phi_i^j$.

\subsubsection{In Two Space Dimensions}

\begin{eqnarray*}
  \parder{J}{u_i^j}
  & = & \parder{}{u_i^j}\det (I + \frac{\partial u_i}{\partial X^j}) \\
  & = & \parder{}{u_i^j} \det 
  \begin{bmatrix}
    1 + \sum_k u^0_k \parder{\phi^0_k}{X^0} & \sum_k u^1_k \parder{\phi^1_k}{X^0}  \\
    \sum_k u^0_k \parder{\phi^0_k}{X^1} & 1 + \sum_k u^1_k \parder{\phi^1_k}{X^1}
  \end{bmatrix} \\
  & = & \parder{}{u_i^j}
  [ ( 1 + \sum_k u^0_k \parder{\phi^0_k}{X^0} ) (1 + \sum_k u^1_k \parder{\phi^1_k}{X^1})
  - (\sum_k u^1_k \parder{\phi^1_k}{X^0}) (\sum_k u^0_k \parder{\phi^0_k}{X^1}) ]
\end{eqnarray*}
Thus,
\begin{eqnarray*}
  \parder{J}{u_i^0}
  & = &
  \parder{\phi_i^0}{X^0} \Big( 1 + \sum_k u^1_k \parder{\phi^1_k}{X^1} \Big)
  -
  \Big(\sum_k u^1_k \parder{\phi^1_k}{X^0} \Big) \parder{\phi_i^0}{X^1} \\
  & = &
  \parder{\phi_i^0}{X^0} \Big( 1 + \parder{u^1_k}{X^1} \Big)
  -
  \parder{u^1_k}{X^0} \parder{\phi_i^0}{X^1} \\
  %
  \parder{J}{u_i^1}
  & = &
  \parder{\phi_i^1}{X^1} \Big( 1 + \sum_k u^0_k \parder{\phi^0_k}{X^0} \Big)
  -
  \Big(\sum_k u^0_k \parder{\phi^0_k}{X^1} \Big) \parder{\phi_i^1}{X^0} \\
  & = &
  \parder{\phi_i^1}{X^1} \Big( 1 + \parder{u^0_k}{X^0} \Big)
  -
  \parder{u^0_k}{X^1} \parder{\phi_i^1}{X^0}
\end{eqnarray*}
The four second derivatives are
\begin{eqnarray*}
  \secparder{J}{u_i^0}{u_j^0}
  & = & 0 \\
  %
 \secparder{J}{u_i^0}{u_j^1}
  & = &
  \parder{\phi_i^0}{X^0} \parder{\phi^1_j}{X^1} 
  - \parder{\phi^1_j}{X^0} \parder{\phi^0_i}{X^1}\\
  % 
 \secparder{J}{u_i^1}{u_j^0}
  & = &
  \parder{\phi^0_j}{X^0} \parder{\phi^1_i}{X^1}
  - \parder{\phi_i^1}{X^0} \parder{\phi^0_j}{X^1} \\
  % 
 \secparder{J}{u_i^1}{u_j^1}
  & = & 0
\end{eqnarray*}

\subsection{The Derivatives of $\tr E$}

\begin{eqnarray*}
  \tr E = \frac 12 \tr (\nabla u + \nabla^T u + \nabla^T u \nabla u)
  = \tr \nabla u + \frac 12 \tr  \nabla^T u \nabla u
\end{eqnarray*}
\subsection{First Derivatives of $W$}

\begin{equation*}
  W(u) = \frac{\lambda}{4} ( J^2 -1 ) - (\frac \lambda 2 + \mu) \ln J + \mu \tr E
\end{equation*}

Thus,
\begin{eqnarray*}
  \parder{W}{u_i^j}
  & = &
   \frac{\lambda}{4} \parder{}{u_i^j} J^2 
   - (\frac \lambda 2 + \mu) \parder{}{u_i^j} \ln J 
   + \mu \parder{}{u_i^j} \tr E \\
  %
   & = &
   \frac{\lambda J}{2} \parder{J}{u_i^j}
   - (\frac \lambda 2 + \mu) J^{-1} \parder{J}{u_i^j}
   + \mu \tr \parder{}{u_i^j} E
\end{eqnarray*}

\subsection{Second Derivatives of $W$}

\begin{eqnarray*}
  \secparder{W}{u_i^j}{u_k^l}
  & = &
  \parder{}{u_k^l} \Big[ \frac{\lambda J}{2} \parder{J}{u_i^j}
  - (\frac \lambda 2 + \mu) J^{-1} \parder{J}{u_i^j}
  + \mu \tr \parder{}{u_i^j} E \Big] \\
  % 
  & = & 
  \frac{\lambda}{2} \Bigg[ \parder{J}{u_i^j} \parder{J}{u_k^l} 
  + J \secparder{J}{u_i^j}{u_k^l} \Bigg] \\
  & &
  - (\frac \lambda 2 + \mu) J^{-2} 
  \Bigg[ \secparder{J}{u_i^j}{u_k^l} J - \parder{J}{u_i^j} \parder{J}{u_k^l} \Bigg]
  + \mu \tr \secparder{}{u_i^j}{u_k^l} E
\end{eqnarray*}
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
